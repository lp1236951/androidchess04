package com.example.chess;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * 
 * @author Anand
 * Tile Object has three fields
 * Field x and y are the location of tile and Field String is the name of object if it has one
 */
public class Tile  implements Serializable{
	
	public int x;
	public int y;
	public String name;
	public Tile(int x, int y, String name){
		this.x = x;
		this.y = y;
		this.name = name;
	}
	/**
	 * Gets all moves possible from this tile give the current game board
	 * Not implemented for Tile because it has no object but will be overridden 
	 * @param game - game board
	 * @return ArrayList<Tile>
	 */
	public ArrayList<Tile> moves(Tile[][] game){
		return null;
	}
	/**
	 * Checks equality of this tile and a given tile
	 * @param n - tile that is being compared to this one
	 * @return boolean
	 */
	public boolean equals(Tile n){
		
		if(n.x == x && n.y == y){
			return true;
		}
		return false;
	}
	/**
	 * Checks if the King is in check given the current board state
	 * @param game - current state of board
	 * @param King - king object that is being checked
	 * @return
	 */
	public boolean check(Tile[][] game, Tile King){
		int x = King.x;
		int y = King.y;
		if(King.name.charAt(0) == 'w'){
			for(int i = x+1; i < 8; i++){
				if(game[i][y].name.contains("bR") || game[i][y].name.contains("bQ")){
					return true;
				}
				if(game[i][y].name != ""){
					break;
				}
			}
			for(int i = x-1; i >= 0; i--){
				if(game[i][y].name.contains("bR") || game[i][y].name.contains("bQ")){
					return true;
				}
				if(game[i][y].name != ""){
					break;
				}
			}
			for(int i = y+1; i < 8; i++){
				if(game[x][i].name.contains("bR") || game[x][i].name.contains("bQ")){
					return true;
				}
				if(game[x][i].name != ""){
					break;
				}
			}
			for(int i = y-1; i >= 0; i--){
				if(game[x][i].name.contains("bR") || game[x][i].name.contains("bQ")){
					return true;
				}
				if(game[x][i].name != ""){
					break;
				}
			}
			for(int i = x+1, k = y+1; i < 8; k++, i++){
				if(k >= 8){
					break;
				}
				if(game[i][k].name.contains("bB") || game[i][k].name.contains("bQ")){
					return true;
				}
				if(game[i][k].name != ""){
					break;
				}
			}
			for(int i = x-1, k = y+1; i >= 0; k++, i--){
				if(k >= 8){
					break;
				}
				if(game[i][k].name.contains("bB") || game[i][k].name.contains("bQ")){
					return true;
				}
				if(game[i][k].name != ""){
					break;
				}
			}
			for(int i = x+1, k = y-1; i < 8; k--, i++){
				if(k < 0){
					break;
				}
				if(game[i][k].name.contains("bB") || game[i][k].name.contains("bQ")){
					return true;
				}
				if(game[i][k].name != ""){
					break;
				}
			}
			for(int i = x-1, k = y-1; i >= 0; k--, i--){
				if(k < 0){
					break;
				}
				if(game[i][k].name.contains("bB") || game[i][k].name.contains("bQ")){
					return true;
				}
				if(game[i][k].name != ""){
					break;
				}
			}
			if(x-1 >= 0 && y-1 >= 0){
				if(game[x-1][y-1].name.contains("bp") || game[x-1][y-1].name.contains("bK")){
					return true;
				}
			}
			if(x-1 >= 0 && y+1 < 8){
				if(game[x-1][y+1].name.contains("bp") || game[x-1][y+1].name.contains("bK")){
					return true;
				}
			}
			
		}else{
			for(int i = x+1; i < 8; i++){
				if(game[i][y].name.contains("wR") || game[i][y].name.contains("wQ")){
					return true;
				}
				if(game[i][y].name != ""){
					break;
				}
			}
			for(int i = x-1; i >= 0; i--){
				if(game[i][y].name.contains("wR") || game[i][y].name.contains("wQ")){
					return true;
				}
				if(game[i][y].name != ""){
					break;
				}
			}
			for(int i = y+1; i < 8; i++){
				if(game[x][i].name.contains("wR") || game[x][i].name.contains("wQ")){
					
					return true;
				}
				if(game[x][i].name != ""){
					break;
				}
			}
			for(int i = y-1; i >= 0; i--){
				if(game[x][i].name.contains("wR") || game[x][i].name.contains("wQ")){
					//System.out.println("here");
					//System.out.println(x+" "+i);
					return true;
				}
				if(game[x][i].name.length() != 0){
					break;
				}
			}
			for(int i = x+1, k = y+1; i < 8; k++, i++){
				if(k >= 8){
					break;
				}
				if(game[i][k].name.contains("wB") || game[i][k].name.contains("wQ")){
					return true;
				}
				if(game[i][k].name != ""){
					break;
				}
			}
			for(int i = x-1, k = y+1; i >= 0; k++, i--){
				if(k >= 8){
					break;
				}
				if(game[i][k].name.contains("wB") || game[i][k].name.contains("wQ")){
					return true;
				}
				if(game[i][k].name != ""){
					break;
				}
			}
			for(int i = x+1, k = y-1; i < 8; k--, i++){
				if(k < 0){
					break;
				}
				if(game[i][k].name.contains("wB") || game[i][k].name.contains("wQ")){
					return true;
				}
				if(game[i][k].name != ""){
					break;
				}
			}
			for(int i = x-1, k = y-1; i >= 0; k--, i--){
				if(k < 0){
					break;
				}
				if(game[i][k].name.contains("wB") || game[i][k].name.contains("wQ")){
					return true;
				}
				if(game[i][k].name != ""){
					break;
				}
			}
			if(x+1 < 8 && y-1 >= 0){
				if(game[x+1][y-1].name.contains("wp") || game[x+1][y-1].name.contains("wK")){
					return true;
				}
			}
			if(x+1 < 8 && y+1 < 8){
				if(game[x+1][y+1].name.contains("wp") || game[x+1][y+1].name.contains("wK")){
					return true;
				}
			}
		}
		return false;
	}

}
