package com.example.chess;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * 
 * @author Anand
 *Bishop object that extends Tile
 */
public class Bishop extends Tile implements Serializable{
/**
 * Constructor
 * @param x
 * @param y
 * @param name
 */
	public Bishop(int x, int y, String name) {
		super(x, y, name);
		// TODO Auto-generated constructor stub
	}
	/**Returns all possible moves for B object given the current state of game
	 * @param game
	 * @return ArrayList<Tile>
	 */
	public ArrayList<Tile> moves(Tile[][] game){
		ArrayList<Tile> r = new ArrayList<Tile>();
		for(int i = x-1, k = y-1; i >= 0; i--,k--){
			if(name.charAt(0) == 'w'){
				if(k < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}else{
				if(k < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}
		}
		
		for(int i = x-1, k = y+1; k < 8; i--,k++){
			if(name.charAt(0) == 'w'){
				if(i < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}else{
				if(i < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}
		}
		for(int i = x+1, k = y-1; i < 8; i++,k--){
			if(name.charAt(0) == 'w'){
				if(k < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}else{
				if(k < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}
		}
		for(int i = x+1, k = y+1; i < 8; i++,k++){
			if(name.charAt(0) == 'w'){
				if(k >= 8){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}else{
				if(k >= 8){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}
		}
		return r;
	}
}
