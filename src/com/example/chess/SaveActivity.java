package com.example.chess;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SaveActivity extends Activity {
	ArrayList<Node> xx;
	Node y;
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_save);
		String winner = getIntent().getStringExtra("Winner").toString();
		final TextView x = (TextView) findViewById(R.id.winner);
		x.setText(winner);
		y = (Node) getIntent().getSerializableExtra("Game");
		xx = (ArrayList<Node>) getIntent().getSerializableExtra("Games");
		
		
		final Button save = (Button) findViewById(R.id.save);
		save.setOnClickListener(new View.OnClickListener() {
			
            public void onClick(View v) {
            	final EditText name = (EditText) findViewById(R.id.name);
            	y.name = name.getText().toString();
            	xx.add(y);
            	x.setText("Game Saved");
            	Intent n = new Intent(SaveActivity.this, HomeActivity.class);
            	n.putExtra("Games", xx);
            	SaveActivity.this.startActivity(n);
            	
            }});
	}
	
}
		