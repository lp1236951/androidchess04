package com.example.chess;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Knight object that extends Tile
 * @author Anand
 *
 */
public class Knight extends Tile implements Serializable{
/**
 * Constructor
 * @param x
 * @param y
 * @param name
 */
	public Knight(int x, int y, String name) {
		super(x, y, name);
		
	}
/**
 * Returns all possible moves for Knight object given game board
 * @param game
 * @return ArrayList<Tile>
 */
	public ArrayList<Tile> moves(Tile[][] game){
		ArrayList<Tile> r = new ArrayList<Tile>();
		if(name.charAt(0) == 'w'){
			if(x+2 < 8 && y+1 < 8){
				if(game[x+2][y+1].name == ""){
				Tile n = new Tile(8-(x+2),y+1,"");	
				r.add(n);
				}else
				if(game[x+2][y+1].name.charAt(0) == 'b' && game[x+2][y+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x+2),y+1,"");	
					r.add(n);
				}
			}
			if(x+2 < 8 && y-1 >= 0){
				if(game[x+2][y-1].name == ""){
					Tile n = new Tile(8-(x+2),y-1,"");	
					r.add(n);
					}else
					if(game[x+2][y-1].name.charAt(0) == 'b' && game[x+2][y-1].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x+2),y-1,"");	
						r.add(n);
					}
			}
			if(x-2 >= 0 && y+1 < 8){
				if(game[x-2][y+1].name == ""){
					Tile n = new Tile(8-(x-2),y+1,"");	
					r.add(n);
					}else
					if(game[x-2][y+1].name.charAt(0) == 'b' && game[x-2][y+1].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x-2),y+1,"");	
						r.add(n);
					}
			}
			if(x-2 >= 0 && y-1 >= 0){
				if(game[x-2][y-1].name == ""){
					Tile n = new Tile(8-(x-2),y-1,"");	
					r.add(n);
					}else
					if(game[x-2][y-1].name.charAt(0) == 'b' && game[x-2][y-1].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x-2),y-1,"");	
						r.add(n);
					}
			}
			if(x+1 < 8 && y+2 < 8){
				if(game[x+1][y+2].name == ""){
					Tile n = new Tile(8-(x+1),y+2,"");	
					r.add(n);
					}else
					if(game[x+1][y+2].name.charAt(0) == 'b' && game[x+1][y+2].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x+1),y+2,"");	
						r.add(n);
					}
			}
			if(x+1 < 8 && y-2 >= 0){
				if(game[x+1][y-2].name == ""){
					Tile n = new Tile(8-(x+1),y-2,"");	
					r.add(n);
					}else
					if(game[x+1][y-2].name.charAt(0) == 'b' && game[x+1][y-2].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x+1),y-2,"");	
						r.add(n);
					}
			}
			if(x-1 >= 0 && y+2 < 8){
				if(game[x-1][y+2].name == ""){
					Tile n = new Tile(8-(x-1),y+2,"");	
					r.add(n);
					}else
					if(game[x-1][y+2].name.charAt(0) == 'b' && game[x-1][y+2].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x-1),y+2,"");	
						r.add(n);
					}
			}
			if(x-1 >= 0 && y-2 >= 0){
				if(game[x-1][y-2].name == ""){
					Tile n = new Tile(8-(x-1),y-2,"");	
					r.add(n);
					}else
					if(game[x-1][y-2].name.charAt(0) == 'b' && game[x-1][y-2].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x-1),y-2,"");	
						r.add(n);
					}
			}
		}else{
			if(x+2 < 8 && y+1 < 8){
				if(game[x+2][y+1].name == ""){
				Tile n = new Tile(8-(x+2),y+1,"");	
				r.add(n);
				}else
				if(game[x+2][y+1].name.charAt(0) == 'w' && game[x+2][y+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x+2),y+1,"");	
					r.add(n);
				}
			}
			if(x+2 < 8 && y-1 >= 0){
				if(game[x+2][y-1].name == ""){
					Tile n = new Tile(8-(x+2),y-1,"");	
					r.add(n);
					}else
					if(game[x+2][y-1].name.charAt(0) == 'w' && game[x+2][y-1].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x+2),y-1,"");	
						r.add(n);
					}
			}
			if(x-2 >= 0 && y+1 < 8){
				if(game[x-2][y+1].name == ""){
					Tile n = new Tile(8-(x-2),y+1,"");	
					r.add(n);
					}else
					if(game[x-2][y+1].name.charAt(0) == 'w' && game[x-2][y+1].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x-2),y+1,"");	
						r.add(n);
					}
			}
			if(x-2 >= 0 && y-1 >= 0){
				if(game[x-2][y-1].name == ""){
					Tile n = new Tile(8-(x-2),y-1,"");	
					r.add(n);
					}else
					if(game[x-2][y-1].name.charAt(0) == 'w' && game[x-2][y-1].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x-2),y-1,"");	
						r.add(n);
					}
			}
			if(x+1 < 8 && y+2 < 8){
				if(game[x+1][y+2].name == ""){
					Tile n = new Tile(8-(x+1),y+2,"");	
					r.add(n);
					}else
					if(game[x+1][y+2].name.charAt(0) == 'w' && game[x+1][y+2].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x+1),y+2,"");	
						r.add(n);
					}
			}
			if(x+1 < 8 && y-2 >= 0){
				if(game[x+1][y-2].name == ""){
					Tile n = new Tile(8-(x+1),y-2,"");	
					r.add(n);
					}else
					if(game[x+1][y-2].name.charAt(0) == 'w' && game[x+1][y-2].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x+1),y-2,"");	
						r.add(n);
					}
			}
			if(x-1 >= 0 && y+2 < 8){
				if(game[x-1][y+2].name == ""){
					Tile n = new Tile(8-(x-1),y+2,"");	
					r.add(n);
					}else
					if(game[x-1][y+2].name.charAt(0) == 'w' && game[x-1][y+2].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x-1),y+2,"");	
						r.add(n);
					}
			}
			if(x-1 >= 0 && y-2 >= 0){
				if(game[x-1][y-2].name == ""){
					Tile n = new Tile(8-(x-1),y-2,"");	
					r.add(n);
					}else
					if(game[x-1][y-2].name.charAt(0) == 'w' && game[x-1][y-2].name.charAt(1) != 'K'){
						Tile n = new Tile(8-(x-1),y-2,"");	
						r.add(n);
					}
			}
		}
		
		
		return r;
	}
}
