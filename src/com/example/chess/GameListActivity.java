package com.example.chess;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class GameListActivity extends Activity {
	ArrayList<Node> xx;
	private ListView z;
	ArrayAdapter<String> arrayAdapter;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gamelist);
		
		xx = (ArrayList<Node>) getIntent().getSerializableExtra("Games");
		z = (ListView) findViewById(R.id.gameslist);
		
		List<String> zz = new ArrayList<String>();
		if(xx != null){
		for(int i = 0; i < xx.size(); i++){
			zz.add(xx.get(i).name);
		}
		
		}else{
			zz.add("No Games");
		}
		
		arrayAdapter = new ArrayAdapter<String>(
                this, 
                android.R.layout.simple_list_item_1,
                zz );

        z.setAdapter(arrayAdapter); 
        
        final ListView list = (ListView) findViewById(R.id.gameslist);
        list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				String gamename = arrayAdapter.getItem(position);
				ArrayList<Node> game;
				for(int i = 0; i < xx.size(); i++){
					if(gamename.compareTo(xx.get(i).name) == 0){
						game = xx.get(i).all;
						Intent n = new Intent(GameListActivity.this, playgame_activity.class);
		            	n.putExtra("Games", game);
		            	GameListActivity.this.startActivity(n);
					}
				}
				
			}
        });

        
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.sort_by_name) {
			boolean t = false;
			ArrayList<String> x = new ArrayList<String>();
			for(int i = 0; i < xx.size(); i++){
				t = false;
				String g = xx.get(i).name;
				if(x.size() == 0){
					x.add(g);
					continue;
				}else{
					for(int k = 0; k < x.size(); k++){
						if(x.get(k).compareTo(g) > 0){
							x.add(k,g);
							t= true;
							break;
						}
					}
				}
				if(t == false){
					x.add(g);
					
				}
			}
			List<String> zz = x;
			arrayAdapter = new ArrayAdapter<String>(
	                this, 
	                android.R.layout.simple_list_item_1,
	                zz );

	        z.setAdapter(arrayAdapter); 
	        
			return true;
		}
		if (id == R.id.sort_by_recent) {
			List<String> zz = new ArrayList<String>();
			if(xx != null){
			for(int i = 0; i < xx.size(); i++){
				zz.add(xx.get(i).name);
			}
			
			}else{
				zz.add("No Games");
			}
			
			arrayAdapter = new ArrayAdapter<String>(
	                this, 
	                android.R.layout.simple_list_item_1,
	                zz );

	        z.setAdapter(arrayAdapter); 
	        
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}