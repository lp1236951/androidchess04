package com.example.chess;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class GameActivity extends Activity {
	
	Tile[][] board;
	Tile new1;
	Tile new2;
	Node front;
	ArrayList<Node> xx;
	@SuppressWarnings("unchecked")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gameboard);	
		final Game game = new Game();
		board = game.Board;
		Tile[][] temp = new Tile[8][8];
    	for(int i = 0; i < 8; i++){
    		for(int k = 0; k < 8; k++){
    			temp[i][k] = board[i][k];
    		}
    	}
		front = new Node(temp, null, null);
		xx = (ArrayList<Node>) getIntent().getSerializableExtra("Games");
		
		final Button hint = (Button) findViewById(R.id.ai);
		hint.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	final TextView who = (TextView) findViewById(R.id.status);
            	String h = who.getText().toString().trim();
            	if(h.contains("White")){
            		for(int i = 0; i < 8; i++){
            			for(int k = 0; k < 8; k++){
            				if(board[i][k].name.contains("w")){
            					ArrayList<Tile> x = board[i][k].moves(board);
            					if(x.size() != 0){
            						Tile t = x.get(0);
            						final TextView set = (TextView) findViewById(R.id.move);
            						String kk = "";
            						String yy = "";
            						if(k == 0){
            							kk = "a";
            						}
            						if(k == 1){
            							kk = "b";
            						}
            						if(k == 2){
            							kk = "c";
            						}
            						if(k == 3){
            							kk = "d";
            						}
            						if(k == 4){
            							kk = "e";
            						}
            						if(k == 5){
            							kk = "f";
            						}
            						if(k == 6){
            							kk = "g";
            						}
            						if(k == 7){
            							kk = "h";
            						}
            						if(t.y == 0){
            							yy = "a";
            						}
            						if(t.y == 1){
            							yy = "b";
            						}
            						if(t.y == 2){
            							yy = "c";
            						}
            						if(t.y == 3){
            							yy = "d";
            						}
            						if(t.y == 4){
            							yy = "e";
            						}
            						if(t.y == 5){
            							yy = "f";
            						}
            						if(t.y == 6){
            							yy = "g";
            						}
            						if(t.y == 7){
            							yy = "h";
            						}
            						i = 8-i;
            						int xx = 8-t.x;
            						String z = kk+""+i+" "+yy+""+xx;
            						set.setText("HINT: "+z);
            						return;
            					}
            				}
            			}
            		}
            	}else{
            		for(int i = 0; i < 8; i++){
            			for(int k = 0; k < 8; k++){
            				if(board[i][k].name.contains("b")){
            					ArrayList<Tile> x = board[i][k].moves(board);
            					if(x.size() != 0){
            						Tile t = x.get(0);
            						final TextView set = (TextView) findViewById(R.id.move);
            						String kk = "";
            						String yy = "";
            						if(k == 0){
            							kk = "a";
            						}
            						if(k == 1){
            							kk = "b";
            						}
            						if(k == 2){
            							kk = "c";
            						}
            						if(k == 3){
            							kk = "d";
            						}
            						if(k == 4){
            							kk = "e";
            						}
            						if(k == 5){
            							kk = "f";
            						}
            						if(k == 6){
            							kk = "g";
            						}
            						if(k == 7){
            							kk = "h";
            						}
            						if(t.y == 0){
            							yy = "a";
            						}
            						if(t.y == 1){
            							yy = "b";
            						}
            						if(t.y == 2){
            							yy = "c";
            						}
            						if(t.y == 3){
            							yy = "d";
            						}
            						if(t.y == 4){
            							yy = "e";
            						}
            						if(t.y == 5){
            							yy = "f";
            						}
            						if(t.y == 6){
            							yy = "g";
            						}
            						if(t.y == 7){
            							yy = "h";
            						}
            						i = 8-i;
            						int xx = 8-t.x;
            						String z = kk+""+i+" "+yy+""+t.x;
            						set.setText("HINT: "+z);
            						return;
            					}
            				}
            			}
            		}
            	}
            	
            }});
		
		
		final Button resign = (Button) findViewById(R.id.quit);
		resign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	final TextView who = (TextView) findViewById(R.id.status);
            	String h = who.getText().toString().trim();
            	while(true){
        			if(front.prev != null){
        				front = front.prev;
        			}else{
        				break;
        			}
        		}
            	front.set();
            	if(h.contains("White")){
            		who.setText("BLACK WINS");
            		Intent n = new Intent(GameActivity.this, SaveActivity.class);
                	n.putExtra("Game", front);
                	n.putExtra("Winner", "Black Wins");
                	n.putExtra("Games", xx);
                	GameActivity.this.startActivity(n);
            	}else{
            		who.setText("WHITE WINS");
            		Intent n = new Intent(GameActivity.this, SaveActivity.class);
                	n.putExtra("Game", front);
                	n.putExtra("Winner", "White Wins");
                	n.putExtra("Games", xx);
                	GameActivity.this.startActivity(n);
            	}
            	
            }});
		final Button draw = (Button) findViewById(R.id.draw);
		draw.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	final TextView whoo = (TextView) findViewById(R.id.move);
                if(whoo.getText().toString().trim().contains("Draw")){
                	while(true){
            			if(front.prev != null){
            				front = front.prev;
            			}else{
            				break;
            			}
            		}
                	front.set();
                    	Intent n = new Intent(GameActivity.this, SaveActivity.class);
                    	n.putExtra("Game", front);
                    	n.putExtra("Winner", "Draw");
                    	n.putExtra("Games", xx);
                    	GameActivity.this.startActivity(n);
                    	
                }else{
                	whoo.setText("Draw Requested");
                }
            	
            }});
		
		
		final Button undo = (Button) findViewById(R.id.undo);
		undo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if(front.prev == null){
            		return;
            	}
            	final TextView who = (TextView) findViewById(R.id.status);
            	String hhh = who.getText().toString().trim();
            	
            	final TextView whoo = (TextView) findViewById(R.id.move);
            
            	whoo.setText("");
            	if(hhh.contains("White")){
            		who.setText("Black Move");
            	}else{
            		who.setText("White Move");
            	}
            	front = front.prev;
            	front.next = null;
            	Tile[][] temp = front.board;
            	for(int i = 0; i < 8; i++){
            		for(int k = 0; k < 8; k++){
            			board[i][k] = temp[i][k];
            		}
            	}
            	
            	ImageView h = (ImageView) findViewById(R.id.p00);
            	if(board[0][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p01);
            	if(board[0][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p02);
            	if(board[0][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p03);
            	if(board[0][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p04);
            	if(board[0][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p05);
            	if(board[0][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p06);
            	if(board[0][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p07);
            	if(board[0][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	
            	h = (ImageView) findViewById(R.id.p10);
            	if(board[1][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p11);
            	if(board[1][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p12);
            	if(board[1][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p13);
            	if(board[1][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p14);
            	if(board[1][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p15);
            	if(board[1][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p16);
            	if(board[1][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p17);
            	if(board[1][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p20);
            	if(board[2][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p21);
            	if(board[2][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p22);
            	if(board[2][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p23);
            	if(board[2][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p24);
            	if(board[2][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p25);
            	if(board[2][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p26);
            	if(board[2][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p27);
            	if(board[2][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p30);
            	if(board[3][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p31);
            	if(board[3][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p32);
            	if(board[3][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p33);
            	if(board[3][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p34);
            	if(board[3][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p35);
            	if(board[3][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p36);
            	if(board[3][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p37);
            	if(board[3][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p40);
            	if(board[4][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p41);
            	if(board[4][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p42);
            	if(board[4][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p43);
            	if(board[4][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p44);
            	if(board[4][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p45);
            	if(board[4][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p46);
            	if(board[4][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p47);
            	if(board[4][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p50);
            	if(board[5][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p51);
            	if(board[5][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p52);
            	if(board[5][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p53);
            	if(board[5][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p54);
            	if(board[5][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p55);
            	if(board[5][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p56);
            	if(board[5][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p57);
            	if(board[5][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p60);
            	if(board[6][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p61);
            	if(board[6][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p62);
            	if(board[6][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p63);
            	if(board[6][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p64);
            	if(board[6][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p65);
            	if(board[6][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p66);
            	if(board[6][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p67);
            	if(board[6][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p70);
            	if(board[7][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p71);
            	if(board[7][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p72);
            	if(board[7][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p73);
            	if(board[7][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p74);
            	if(board[7][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p75);
            	if(board[7][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p76);
            	if(board[7][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p77);
            	if(board[7][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            }});
            
		final Button movebutton = (Button) findViewById(R.id.moveButton);
        movebutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	final TextView y = (TextView) findViewById(R.id.move);
    			y.setText("");
            	final EditText text = (EditText) findViewById(R.id.entermove);
            	String x = text.getText().toString().trim();
            	Tile[][] temp = front.board;
            	for(int i = 0; i < 8; i++){
            		for(int k = 0; k < 8; k++){
            			board[i][k] = temp[i][k];
            		}
            	}
            	Tile whiteking = new Tile(7,4,"wK");
        		Tile blackking = new Tile(0,4,"bK");
        		for(int i = 0; i < 8; i++){
        			for(int k = 0; k < 8; k++){
        				if(board[i][k].name == "wK"){
        					whiteking = new King(i,k,"wk");
        				}
        				if(board[i][k].name == "bK"){
        					blackking = new King(i,k,"bk");
        				}
        			}
        		}
        		
            	boolean a = action(board ,x);
            	if(a){
            		final TextView who = (TextView) findViewById(R.id.status);
            		String hhh = who.getText().toString().trim();
            		
            		final TextView c = (TextView) findViewById(R.id.move);
            		String hh = c.getText().toString().trim();
            		boolean check = false;
            		boolean white = false;
            		if(hh.contains("check")){
            			check = true;
            		}
            		
            		if(hhh.contains("White")){
            			white = true;
            		}else{
            			white = false;
            		}
            		
            		boolean chmate = false;
            		if(check){
            		if(white){
            			chmate = checkmate(board,whiteking);
            			if(chmate){
            				final TextView whoo = (TextView) findViewById(R.id.status);
                        	String ho = who.getText().toString().trim();
                        	while(true){
                    			if(front.prev != null){
                    				front = front.prev;
                    			}else{
                    				break;
                    			}
                    		}
                        	front.set();
                        		who.setText("BLACK WINS");
                            	Intent nn = new Intent(GameActivity.this, SaveActivity.class);
                                nn.putExtra("Game", front);
                                nn.putExtra("Winner", "Black WINS");
                                nn.putExtra("Games", xx);
                                GameActivity.this.startActivity(nn);
                         
            			}
            		}else{
            			chmate = checkmate(board,blackking);
            			if(chmate){
            				final TextView whoo = (TextView) findViewById(R.id.status);
                        	String ho = who.getText().toString().trim();
                        	while(true){
                    			if(front.prev != null){
                    				front = front.prev;
                    			}else{
                    				break;
                    			}
                    		}
                        	front.set();
                        	who.setText("WHITE WINS");
                        	Intent nn = new Intent(GameActivity.this, SaveActivity.class);
                            nn.putExtra("Game", front);
                            nn.putExtra("Winner", "White WINS");
                            nn.putExtra("Games", xx);
                            GameActivity.this.startActivity(nn);
                            
            			}
            				
            		}
            		}
            		
            		Tile[][] tt = new Tile[8][8];
                	for(int i = 0; i < 8; i++){
                		for(int k = 0; k < 8; k++){
                			tt[i][k] = board[i][k];
                		}
                	}
            		Node n = new Node(tt, null, null);
            		front.next = n;
            		n.prev = front;
            		front = front.next;
            	final TextView whooo = (TextView) findViewById(R.id.status);
            	
            	if(whooo.getText().toString().trim().contains("White") && whooo.getText().toString().contains("WINS") == false){
            		whooo.setText("Black Move");
            	}else{
            		whooo.setText("White Move");
            	}
            	
            	ImageView h = (ImageView) findViewById(R.id.p00);
            	if(board[0][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p01);
            	if(board[0][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p02);
            	if(board[0][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p03);
            	if(board[0][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p04);
            	if(board[0][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p05);
            	if(board[0][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p06);
            	if(board[0][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p07);
            	if(board[0][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	
            	h = (ImageView) findViewById(R.id.p10);
            	if(board[1][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p11);
            	if(board[1][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p12);
            	if(board[1][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p13);
            	if(board[1][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p14);
            	if(board[1][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p15);
            	if(board[1][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p16);
            	if(board[1][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p17);
            	if(board[1][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p20);
            	if(board[2][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p21);
            	if(board[2][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p22);
            	if(board[2][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p23);
            	if(board[2][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p24);
            	if(board[2][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p25);
            	if(board[2][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p26);
            	if(board[2][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p27);
            	if(board[2][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p30);
            	if(board[3][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p31);
            	if(board[3][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p32);
            	if(board[3][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p33);
            	if(board[3][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p34);
            	if(board[3][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p35);
            	if(board[3][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p36);
            	if(board[3][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p37);
            	if(board[3][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p40);
            	if(board[4][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p41);
            	if(board[4][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p42);
            	if(board[4][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p43);
            	if(board[4][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p44);
            	if(board[4][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p45);
            	if(board[4][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p46);
            	if(board[4][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p47);
            	if(board[4][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p50);
            	if(board[5][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p51);
            	if(board[5][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p52);
            	if(board[5][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p53);
            	if(board[5][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p54);
            	if(board[5][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p55);
            	if(board[5][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p56);
            	if(board[5][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p57);
            	if(board[5][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p60);
            	if(board[6][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p61);
            	if(board[6][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p62);
            	if(board[6][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p63);
            	if(board[6][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p64);
            	if(board[6][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p65);
            	if(board[6][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p66);
            	if(board[6][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p67);
            	if(board[6][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p70);
            	if(board[7][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p71);
            	if(board[7][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p72);
            	if(board[7][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p73);
            	if(board[7][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p74);
            	if(board[7][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p75);
            	if(board[7][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p76);
            	if(board[7][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p77);
            	if(board[7][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	
            	
            	}
            }
        });
		
	}
	
	public boolean action(Tile[][] game, String x){
		boolean white = true;
		boolean check = false;
		boolean wkm = false;
		boolean bkm = false;
		boolean Final = false;
		Tile en = new Tile(8,8,"");
		Tile whiteking = new Tile(7,4,"wK");
		Tile blackking = new Tile(0,4,"bK");
		Tile checker;
		
		for(int i = 0; i < 8; i++){
			for(int k = 0; k < 8; k++){
				if(game[i][k].name == "wK"){
					whiteking = new King(i,k,"wk");
				}
				if(game[i][k].name == "bK"){
					blackking = new King(i,k,"bk");
				}
			}
		}
		final TextView who = (TextView) findViewById(R.id.status);
		String h = who.getText().toString().trim();
		
		final TextView c = (TextView) findViewById(R.id.move);
		String hh = c.getText().toString().trim();
		
		if(hh.contains("check")){
			check = true;
		}
		
		if(h.contains("White")){
			white = true;
		}else{
			white = false;
		}
		
		boolean chmate = false;
		
		

		int c1 = 0;
		int c2 = 0;
		int e1 = 0;
		int e2 = 0;

		if(x.length() < 5){
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
		
		String curr = x.substring(0, 2);
		String end = x.substring(3);
		String request = "";
		if(end.length() > 2){
			end = end.substring(0, 2);
			request = x.substring(5).trim();
		   
		}
		
		c1 = Character.getNumericValue(curr.charAt(1));
		e1 = Character.getNumericValue(end.charAt(1));
		if(c1 >= 9 || c1 < 1 || e1 >= 9 || e1 < 1){
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
		
		switch(curr.charAt(0)){
		case 'a': c2 = 0; break;
		case 'b': c2 = 1; break;
		case 'c': c2 = 2; break;
		case 'd': c2 = 3; break;
		case 'e': c2 = 4; break;
		case 'f': c2 = 5; break;
		case 'g': c2 = 6; break;
		case 'h': c2 = 7; break;
		default:
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
		switch(end.charAt(0)){
		case 'a': e2 = 0; break;
		case 'b': e2 = 1; break;
		case 'c': e2 = 2; break;
		case 'd': e2 = 3; break;
		case 'e': e2 = 4; break;
		case 'f': e2 = 5; break;
		case 'g': e2 = 6; break;
		case 'h': e2 = 7; break;
		default:
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
		if(c1 == e1 && c2 == e2){
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
		c1 = 8 - c1;
		e1 = 8 - e1;
		new1 = new Tile(e1,e2, game[e1][e2].name);
		new2 = new Tile(c1,c2, "");
		if(game[c1][c2].name == ""){
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
		if(game[c1][c2].name.contains("b") && white == true){
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
		if(game[c1][c2].name.contains("w") && white == false){
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
		
		if(game[c1][c2].name.charAt(1) == 'K'){
			if(e1 == 7 && e2 == 2){
				if(game[7][0].name == "wR" && wkm == false){
					if(game[7][1].name == "" && game[7][2].name == "" && game[7][3].name == ""){
						game[7][3] = new Castle(7,3,"wR");
						game[7][2] = new King(7,2,"wK");
						whiteking = new King(7,2,"wK");
						game[7][0] = new Tile(7,0,"");
						game[7][4] = new Tile(7,4,"");
						
						if(white){
							white = false;
						}else{
							white = true;
						}
						return true;
					}
				}
			}
			if(e1 == 7 && e2 == 6){
				if(game[7][7].name == "wR" && wkm == false){
					if(game[7][6].name == "" && game[7][5].name == ""){
						game[7][5] = new Castle(7,5,"wR");
						game[7][6] = new King(7,6,"wK");
						whiteking = new King(7,6,"wK");
						game[7][7] = new Tile(7,0,"");
						game[7][4] = new Tile(7,4,"");
						if(white){
							white = false;
						}else{
							white = true;
						}
						return true;
					}
				}
			}
			if(e1 == 0 && e2 == 6){
				if(game[0][7].name == "bR" && bkm == false){
					if(game[0][6].name == "" && game[0][5].name == ""){
						game[0][5] = new Castle(0,5,"bR");
						game[0][6] = new King(0,6,"bK");
						blackking = new King(0,6,"bK");
						game[0][7] = new Tile(0,0,"");
						game[0][4] = new Tile(0,4,"");
						if(white){
							white = false;
						}else{
							white = true;
						}
						return true;
					}
				}
			}
			if(e1 == 0 && e2 == 2){
				if(game[0][0].name == "bR" && wkm == false){
					if(game[0][1].name == "" && game[0][2].name == "" && game[0][3].name == ""){
						game[0][3] = new Castle(0,3,"bR");
						game[0][2] = new King(0,2,"bK");
						blackking = new King(0,2,"bK");
						game[0][0] = new Tile(0,0,"");
						game[0][4] = new Tile(0,4,"");
						if(white){
							white = false;
						}else{
							white = true;
						}
						return true;
					}
				}
			}
		}
		ArrayList<Tile> moves = game[c1][c2].moves(game);
		boolean possible = false;
		
		//System.out.println(moves.size());
		//System.out.println(e1+" "+e2+" RE");
		for(int i = 0; i < moves.size(); i++){
			Tile n = moves.get(i);
	    // 	System.out.println(8-n.x+" "+n.y);
			
			if(8-n.x == e1 && n.y == e2){
				possible = true;
				
			}
			if(game[c1][c2].name.charAt(1) == 'p' && (c1 == 3 || c1 == 4)){
				if(en.x == e1 && en.y == e2){
					possible = true;
				}
			}
			
		}
		if(moves.size() == 0 && possible == false){
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
		//System.out.println(game[c1][c2].name);
		//System.out.println(game[e1][e2].name);
		checker = game[e1][e2];
		if(game[c1][c2].name.charAt(1) == 'p'){
		if(possible){
			game[e1][e2] = new Pond(e1,e2, game[c1][c2].name.toString());
			if(en.x == e1 && en.y == e2){
				game[c1][c2] = new Tile(c1,c2,"");
				if(game[e1][e2].name.charAt(0) == 'w'){
					game[e1+1][e2] = new Tile(e1+1,e2,"");
				}else{
					game[e1-1][e2] = new Tile(e1+1,e2,"");
				}
			}
			if(e1-c1 == 2 || e1-c1 == -2){
				if(game[e1][e2].name.charAt(0) == 'w'){
					en = new Pond(e1+1,e2, "wp");
				}else{
					en = new Pond(e1-1,e2,"bp");
				}
			}
			if(e1 == 0){
				if(request.contains("R")){
					game[e1][e2] = new Castle(e1,e2, "wR");
				}else if(request.contains("N")){
					game[e1][e2] = new Knight(e1,e2, "wN");
				}else if(request.contains("B")){
					game[e1][e2] = new Bishop(e1,e2, "wB");
				}else if(request.contains("Q")){
					game[e1][e2] = new Queen(e1,e2, "wQ");
				}else{
					game[e1][e2] = new Queen(e1,e2, "wQ");
				}
			}
			if(e1 == 7){
				if(request.contains("R")){
					game[e1][e2] = new Castle(e1,e2, "bR");
				}else if(request.contains("N")){
					game[e1][e2] = new Knight(e1,e2, "bN");
				}else if(request.contains("B")){
					game[e1][e2] = new Bishop(e1,e2, "bB");
				}else if(request.contains("Q")){
					game[e1][e2] = new Queen(e1,e2, "bQ");
				}else{
					game[e1][e2] = new Queen(e1,e2, "bQ");
				}
			}
			game[c1][c2] = new Tile(c1,c2,"");
			if(white){
				white = false;
			}else{
				white = true;
			}
			boolean w = check;
			if(white){
				if(w){
					check = blackking.check(game, blackking);
					if(check){
						game[c1][c2] = game[e1][e2];
						game[e1][e2] = checker;
						final TextView y = (TextView) findViewById(R.id.move);
						y.setText("Illegal Move");
						if(white){
							white = false;
						}else{
							white = true;
						}
						return false;
					}
				}
				check = whiteking.check(game, whiteking);
				}else{
					if(w){
						check = whiteking.check(game, whiteking);
						if(check){
							game[c1][c2] = game[e1][e2];
							game[e1][e2] = checker;
							final TextView y = (TextView) findViewById(R.id.move);
							y.setText("Illegal Move");
							if(white){
								white = false;
							}else{
								white = true;
							}
							return false;
						}
					}
					check = blackking.check(game, blackking);
				}
			if(check){
				c.setText("check");
			}else{
				c.setText("");
			}
			return true;
		}else{
			final TextView y = (TextView) findViewById(R.id.move);
			y.setText("Illegal Move");
			return false;
		}
	}
		if(game[c1][c2].name.charAt(1) == 'R'){
			if(possible){
				game[e1][e2] = new Castle(e1,e2, game[c1][c2].name.toString());
				
				game[c1][c2] = new Tile(c1,c2,"");
				
				if(white){
					white = false;
				}else{
					white = true;
				}
				
				boolean w = check;
				if(white){
					if(w){
						check = blackking.check(game, blackking);
						if(check){
							game[c1][c2] = game[e1][e2];
							game[e1][e2] = checker;
							final TextView y = (TextView) findViewById(R.id.move);
							y.setText("Illegal Move");
							if(white){
								white = false;
							}else{
								white = true;
							}
							return false;
						}
					}
					check = whiteking.check(game, whiteking);
					}else{
						if(w){
							check = whiteking.check(game, whiteking);
							if(check){
								game[c1][c2] = game[e1][e2];
								game[e1][e2] = checker;
								final TextView y = (TextView) findViewById(R.id.move);
								y.setText("Illegal Move");
								if(white){
									white = false;
								}else{
									white = true;
								}
								return false;
							}
						}
						check = blackking.check(game, blackking);
					}
				if(check){
					c.setText("check");
				}else{
					c.setText("");
				}
				return true;
			}else{
				final TextView y = (TextView) findViewById(R.id.move);
				y.setText("Illegal Move");
				return false;
			}
		}
		if(game[c1][c2].name.charAt(1) == 'N'){
			if(possible){
				game[e1][e2] = new Knight(e1,e2, game[c1][c2].name.toString());
				
				game[c1][c2] = new Tile(c1,c2,"");
				if(white){
					white = false;
				}else{
					white = true;
				}
				boolean w = check;
				if(white){
					if(w){
						check = blackking.check(game, blackking);
						if(check){
							game[c1][c2] = game[e1][e2];
							game[e1][e2] = checker;
							final TextView y = (TextView) findViewById(R.id.move);
							y.setText("Illegal Move");
							if(white){
								white = false;
							}else{
								white = true;
							}
							return false;
						}
					}
					check = whiteking.check(game, whiteking);
					}else{
						if(w){
							check = whiteking.check(game, whiteking);
							if(check){
								game[c1][c2] = game[e1][e2];
								game[e1][e2] = checker;
								final TextView y = (TextView) findViewById(R.id.move);
								y.setText("Illegal Move");
								if(white){
									white = false;
								}else{
									white = true;
								}
								return false;
							}
						}
						check = blackking.check(game, blackking);
					}
				if(check){
					c.setText("check");
				}else{
					c.setText("");
				}
				return true;
			}else{
				final TextView y = (TextView) findViewById(R.id.move);
				y.setText("Illegal Move");
				return false;
			}
		}
		if(game[c1][c2].name.charAt(1) == 'B'){
			if(possible){
				game[e1][e2] = new Bishop(e1,e2, game[c1][c2].name.toString());
				
				game[c1][c2] = new Tile(c1,c2,"");
				if(white){
					white = false;
				}else{
					white = true;
				}
				boolean w = check;
				if(white){
					if(w){
						check = blackking.check(game, blackking);
						if(check){
							game[c1][c2] = game[e1][e2];
							game[e1][e2] = checker;
							final TextView y = (TextView) findViewById(R.id.move);
							y.setText("Illegal Move");
							if(white){
								white = false;
							}else{
								white = true;
							}
							return false;
						}
					}
					check = whiteking.check(game, whiteking);
					}else{
						if(w){
							check = whiteking.check(game, whiteking);
							if(check){
								game[c1][c2] = game[e1][e2];
								game[e1][e2] = checker;
								final TextView y = (TextView) findViewById(R.id.move);
								y.setText("Illegal Move");
								if(white){
									white = false;
								}else{
									white = true;
								}
								return false;
							}
						}
						check = blackking.check(game, blackking);
					}
				if(check){
					c.setText("check");
				}else{
					c.setText("");
				}
				return true;
			}else{
				final TextView y = (TextView) findViewById(R.id.move);
				y.setText("Illegal Move");
				return false;
			}
		}
		if(game[c1][c2].name.charAt(1) == 'Q'){
			if(possible){
				game[e1][e2] = new Queen(e1,e2, game[c1][c2].name.toString());
				
				game[c1][c2] = new Tile(c1,c2,"");
				
				if(white){
					white = false;
				}else{
					white = true;
				}
				boolean w = check;
				if(white){
					if(w){
						check = blackking.check(game, blackking);
						if(check){
							game[c1][c2] = game[e1][e2];
							game[e1][e2] = checker;
							final TextView y = (TextView) findViewById(R.id.move);
							y.setText("Illegal Move");
							if(white){
								white = false;
							}else{
								white = true;
							}
							return false;
						}
					}
					check = whiteking.check(game, whiteking);
					}else{
						if(w){
							check = whiteking.check(game, whiteking);
							if(check){
								game[c1][c2] = game[e1][e2];
								game[e1][e2] = checker;
								final TextView y = (TextView) findViewById(R.id.move);
								y.setText("Illegal Move");
								if(white){
									white = false;
								}else{
									white = true;
								}
								return false;
							}
						}
						check = blackking.check(game, blackking);
					}
				if(check){
					c.setText("check");
				}else{
					c.setText("");
				}
				return true;
			}else{
				final TextView y = (TextView) findViewById(R.id.move);
				y.setText("Illegal Move");
				return false;
			}
		}
		if(game[c1][c2].name.charAt(1) == 'K'){
			if(possible){
				game[e1][e2] = new King(e1,e2, game[c1][c2].name.toString());
				if(game[e1][e2].name == "wK"){
					whiteking = new Tile(e1,e2,"wK");
					wkm = true;
				}else{
					blackking = new Tile(e1,e2,"bK");
					bkm = true;
				}
				game[c1][c2] = new Tile(c1,c2,"");
				if(white){
					white = false;
				}else{
					white = true;
				}
				boolean w = check;
				if(white){
					if(w){
						check = blackking.check(game, blackking);
						if(check){
							game[c1][c2] = game[e1][e2];
							game[e1][e2] = checker;
							final TextView y = (TextView) findViewById(R.id.move);
							y.setText("Illegal Move");
							if(white){
								white = false;
							}else{
								white = true;
							}
							return false;
						}
					}
					check = whiteking.check(game, whiteking);
					}else{
						if(w){
							check = whiteking.check(game, whiteking);
							if(check){
								game[c1][c2] = game[e1][e2];
								game[e1][e2] = checker;
								final TextView y = (TextView) findViewById(R.id.move);
								y.setText("Illegal Move");
								if(white){
									white = false;
								}else{
									white = true;
								}
								return false;
							}
						}
						check = blackking.check(game, blackking);
					}
				if(check){
					c.setText("check");
				}else{
					c.setText("");
				}
				return true;
			}else{
				final TextView y = (TextView) findViewById(R.id.move);
				y.setText("Illegal Move");
				return false;
			}
		}
		return false;
		
	}
	
	public static boolean checkmate(Tile[][] game, Tile King){
		int x = King.x;
		int y = King.y;
		ArrayList<Tile> checks = new ArrayList<Tile>();
		ArrayList<Tile> Final = new ArrayList<Tile>();
		if(King.name.charAt(0) == 'w'){
			for(int i = x+1; i < 8; i++){
				if(game[i][y].name.contains("bR") || game[i][y].name.contains("bQ")){
					Tile n = new Tile(i,y,"");
					checks.add(n);
					break;
				}
				if(game[i][y].name == ""){
					Tile n = new Tile(i,y,"");
					checks.add(n);
				}
				if(game[i][y].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x-1; i >= 0; i--){
				if(game[i][y].name.contains("bR") || game[i][y].name.contains("bQ")){
					Tile n = new Tile(i,y,"");
					checks.add(n);
					break;
				}
				if(game[i][y].name == ""){
					Tile n = new Tile(i,y,"");
					checks.add(n);
				}
				if(game[i][y].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = y+1; i < 8; i++){
				if(game[x][i].name.contains("bR") || game[x][i].name.contains("bQ")){
					Tile n = new Tile(x,i,"");
					checks.add(n);
					break;
				}
				if(game[x][i].name == ""){
					Tile n = new Tile(x,i,"");
					checks.add(n);
				}
				if(game[x][i].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = y-1; i >= 0; i--){
				if(game[x][i].name.contains("bR") || game[x][i].name.contains("bQ")){
					Tile n = new Tile(x,i,"");
					checks.add(n);
					break;
				}
				if(game[x][i].name == ""){
					Tile n = new Tile(x,i,"");
					checks.add(n);
				}
				if(game[x][i].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x+1, k = y+1; i < 8; k++, i++){
				if(k >= 8){
					break;
				}
				if(game[i][k].name.contains("bB") || game[i][k].name.contains("bQ")){
					Tile n = new Tile(i,k,"");
					checks.add(n);
					break;
				}
				if(game[i][k].name == ""){
					Tile n = new Tile(i,k,"");
					checks.add(n);
				}
				if(game[i][k].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x-1, k = y+1; i >= 0; k++, i--){
				if(k >= 8){
					break;
				}
				if(game[i][k].name.contains("bB") || game[i][k].name.contains("bQ")){
					Tile n = new Tile(i,k,"");
					checks.add(n);
					break;
				}
				if(game[i][k].name == ""){
					Tile n = new Tile(i,k,"");
					checks.add(n);
				}
				if(game[i][k].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x+1, k = y-1; i < 8; k--, i++){
				if(k < 0){
					break;
				}
				if(game[i][k].name.contains("bB") || game[i][k].name.contains("bQ")){
					Tile n = new Tile(i,k,"");
					checks.add(n);
					break;
				}
				if(game[i][k].name == ""){
					Tile n = new Tile(i,k,"");
					checks.add(n);
				}
				if(game[i][k].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x-1, k = y-1; i >= 0; k--, i--){
				if(k < 0){
					break;
				}
				if(game[i][k].name.contains("bB") || game[i][k].name.contains("bQ")){
					Tile n = new Tile(i,k,"");
					checks.add(n);
					break;
				}
				if(game[i][k].name == ""){
					Tile n = new Tile(i,k,"");
					checks.add(n);
				}
				if(game[i][k].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			if(x-1 >= 0 && y-1 >= 0){
				if(game[x-1][y-1].name.contains("bp") || game[x-1][y-1].name.contains("bK")){
					Tile n = new Tile(x-1,y-1,"");
					checks.add(n);
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			if(x-1 >= 0 && y+1 < 8){
				if(game[x-1][y+1].name.contains("bp") || game[x-1][y+1].name.contains("bK")){
					Tile n = new Tile(x-1,y+1,"");
					checks.add(n);
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			
		}else{
			for(int i = x+1; i < 8; i++){
				if(game[i][y].name.contains("wR") || game[i][y].name.contains("wQ")){
					Tile n = new Tile(i,y,"");
					checks.add(n);
					break;
				}
				if(game[i][y].name == ""){
					Tile n = new Tile(i,y,"");
					checks.add(n);
				}
				if(game[i][y].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x-1; i >= 0; i--){
				if(game[i][y].name.contains("wR") || game[i][y].name.contains("wQ")){
					Tile n = new Tile(i,y,"");
					checks.add(n);
					break;
				}
				if(game[i][y].name == ""){
					Tile n = new Tile(i,y,"");
					checks.add(n);
				}
				if(game[i][y].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = y+1; i < 8; i++){
				if(game[x][i].name.contains("wR") || game[x][i].name.contains("wQ")){
					Tile n = new Tile(x,i,"");
					checks.add(n);
					break;
				}
				if(game[x][i].name == ""){
					Tile n = new Tile(x,i,"");
					checks.add(n);
				}
				if(game[x][i].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = y-1; i >= 0; i--){
				if(game[x][i].name.contains("wR") || game[x][i].name.contains("wQ")){
					Tile n = new Tile(x,i,"");
					checks.add(n);
					break;
				}
				if(game[x][i].name == ""){
					Tile n = new Tile(x,i,"");
					checks.add(n);
				}
				if(game[x][i].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x+1, k = y+1; i < 8; k++, i++){
				if(k >= 8){
					break;
				}
				if(game[i][k].name.contains("wB") || game[i][k].name.contains("wQ")){
					Tile n = new Tile(i,k,"");
					checks.add(n);
					break;
				}
				if(game[i][k].name == ""){
					Tile n = new Tile(i,k,"");
					checks.add(n);
				}
				if(game[i][k].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x-1, k = y+1; i >= 0; k++, i--){
				if(k >= 8){
					break;
				}
				if(game[i][k].name.contains("wB") || game[i][k].name.contains("wQ")){
					Tile n = new Tile(i,k,"");
					checks.add(n);
					break;
				}
				if(game[i][k].name == ""){
					Tile n = new Tile(i,k,"");
					checks.add(n);
				}
				if(game[i][k].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x+1, k = y-1; i < 8; k--, i++){
				if(k < 0){
					break;
				}
				if(game[i][k].name.contains("wB") || game[i][k].name.contains("wQ")){
					Tile n = new Tile(i,k,"");
					checks.add(n);
					break;
				}
				if(game[i][k].name == ""){
					Tile n = new Tile(i,k,"");
					checks.add(n);
				}
				if(game[i][k].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			for(int i = x-1, k = y-1; i >= 0; k--, i--){
				if(k < 0){
					break;
				}
				if(game[i][k].name.contains("wB") || game[i][k].name.contains("wQ")){
					Tile n = new Tile(i,k,"");
					checks.add(n);
					break;
				}
				if(game[i][k].name == ""){
					Tile n = new Tile(i,k,"");
					checks.add(n);
				}
				if(game[i][k].name != ""){
					checks.clear();
					break;
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			if(x+1 < 8 && y-1 >= 0){
				if(game[x+1][y-1].name.contains("wp") || game[x+1][y-1].name.contains("wK")){
					Tile n = new Tile(x+1,y-1,"");
					checks.add(n);
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
			if(x+1 < 8 && y+1 < 8){
				if(game[x+1][y+1].name.contains("wp") || game[x+1][y+1].name.contains("wK")){
					Tile n = new Tile(x+1,y+1,"");
					checks.add(n);
				}
			}
			if(checks.size() != 0){
				for(int i = 0; i < checks.size(); i++){
					Final.add(checks.remove(0));
				}
			}
		}
		//System.out.println(Final.size());
		Tile tempk = new King(0,0,"wK");
		if(King.name.charAt(0) == 'w'){
			for(int i = 0; i < 8; i++){
				for(int k = 0; k < 8; k++){
					if(game[i][k].name.contains("b")){
						ArrayList<Tile> moves = game[i][k].moves(game);
						for(int h = 0; h < moves.size(); h++){
							for(int j = 0; j < Final.size(); j++){
								Tile u = moves.get(h);
								Tile z = Final.get(j);
								if(u.x == z.x && u.y == z.y){
									Tile[][] temp = new Tile[8][8];
									for(int q = 0; q < 8; q++){
										for(int v = 0; v < 8; v++){
											temp[q][v] = game[q][v];
										}
									}
									Tile p = temp[z.x][z.y];
									temp[z.x][z.y] = temp[u.x][u.y];
									temp[u.x][u.y] = new Tile(u.x, u.y, "");
									if(temp[z.x][z.y].name.contains("K")){
										tempk = King;
										King = temp[z.x][z.y];
									}
									boolean f = King.check(temp, King);
									if(f == false){
										return false;
									}else{
										temp[u.x][u.y] = temp[z.x][z.y];
										temp[z.x][z.y] = p;
										King = tempk;
									}
								}
							}
						}
					}
				}
			}
		}else{
			tempk = new King(0,0,"bK");
			for(int i = 0; i < 8; i++){
				for(int k = 0; k < 8; k++){
					if(game[i][k].name.contains("w")){
						ArrayList<Tile> moves = game[i][k].moves(game);
						for(int h = 0; h < moves.size(); h++){
							for(int j = 0; j < Final.size(); j++){
								Tile u = moves.get(h);
								Tile z = Final.get(j);
								if(u.x == z.x && u.y == z.y){
									Tile[][] temp = new Tile[8][8];
									for(int q = 0; q < 8; q++){
										for(int v = 0; v < 8; v++){
											temp[q][v] = game[q][v];
										}
									}
									Tile p = temp[z.x][z.y];
									temp[z.x][z.y] = temp[u.x][u.y];
									temp[u.x][u.y] = new Tile(u.x, u.y, "");
									if(temp[z.x][z.y].name.contains("K")){
										tempk = King;
										King = temp[z.x][z.y];
									}
									boolean f = King.check(temp, King);
									if(f == false){
										return false;
									}else{
										temp[u.x][u.y] = temp[z.x][z.y];
										temp[z.x][z.y] = p;
										King = tempk;
									}
								}
							}
						}
					}
				}
			}
		}
		return true;
	}

}