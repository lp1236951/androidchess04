package com.example.chess;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * 
 * @author Brian
 *
 */
public class Castle extends Tile implements Serializable{
/**
 * Constructor
 * @param x
 * @param y
 * @param name
 */
	public Castle(int x, int y, String name) {
		super(x, y, name);
		// TODO Auto-generated constructor stub
	}
	/**
	 * Gets all the possible moves possible by Castle object given the board
	 * @param game
	 * @return ArrayList<Tile>
	 */
	public ArrayList<Tile> moves(Tile[][] game){
		ArrayList<Tile> r = new ArrayList<Tile>();
		
		for(int i = x-1; i >= 0; i--){
			if(name.charAt(0) == 'w'){
				if(game[i][y].name.compareTo("") == 0){

					Tile n = new Tile(8-i,y, "");
					r.add(n);
					continue;
				}
				if(game[i][y].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][y].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][y].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					break;
				}
			}else{
				if(game[i][y].name.compareTo("") == 0){

					Tile n = new Tile(8-i,y, "");
					r.add(n);
					continue;
				}
				if(game[i][y].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][y].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][y].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					break;
				}
				
			}
		}
		
		for(int i = x+1; i < 8; i++){
			if(name.charAt(0) == 'w'){
				if(game[i][y].name.compareTo("") == 0){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					continue;
				}
				if(game[i][y].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][y].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][y].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					break;
				}
				
			}else{
				if(game[i][y].name.compareTo("") == 0){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					continue;
				}
				if(game[i][y].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][y].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][y].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					break;
				}
			}
		}
		for(int i = y-1; i >= 0; i--){
			if(name.charAt(0) == 'w'){
				if(game[x][i].name.compareTo("") == 0){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					continue;
				}
				if(game[x][i].name.charAt(0) == 'w'){
					break;
				}
				if(game[x][i].name.charAt(1) == 'K'){
					break;
				}
				if(game[x][i].name.charAt(0) == 'b'){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					break;
				}
			}else{
				if(game[x][i].name.compareTo("") == 0){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					continue;
				}
				if(game[x][i].name.charAt(0) == 'b'){
					break;
				}
				if(game[x][i].name.charAt(1) == 'K'){
					break;
				}
				if(game[x][i].name.charAt(0) == 'w'){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					break;
				}
			}
		}
		for(int i = y+1; i < 8; i++){
			if(name.charAt(0) == 'w'){
				if(game[x][i].name.compareTo("") == 0){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					continue;
				}
				if(game[x][i].name.charAt(0) == 'w'){
					break;
				}
				if(game[x][i].name.charAt(1) == 'K'){
					break;
				}
				if(game[x][i].name.charAt(0) == 'b'){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					break;
				}
			}else{
				if(game[x][i].name.compareTo("") == 0){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					continue;
				}
				if(game[x][i].name.charAt(0) == 'b'){
					break;
				}
				if(game[x][i].name.charAt(1) == 'K'){
					break;
				}
				if(game[x][i].name.charAt(0) == 'w'){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					break;
				}
			}
		}
		return r;
	}

}
