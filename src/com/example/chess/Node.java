package com.example.chess;

import java.io.Serializable;
import java.util.ArrayList;

public class Node implements Serializable{
	Tile[][] board;
	Node prev;
	Node next;
	String name = "";
	ArrayList<Node> all;
	
	public Node(){
		
	}
	public Node(Tile[][] board, Node next, Node prev){
		this.board = board;
		this.next = next;
		this.prev = prev;
	}
	
	public void set(){
		all = new ArrayList<Node>();
		Node front = next;
		while(front != null){
			all.add(front);
			front = front.next;
		}
	}
}