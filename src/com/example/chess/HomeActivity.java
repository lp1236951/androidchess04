package com.example.chess;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class HomeActivity extends Activity {
	ArrayList<Node> xx;
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		xx = (ArrayList<Node>) getIntent().getSerializableExtra("Games");
		if(xx == null){
			xx = new ArrayList<Node>();
		}
		final Button newgame = (Button) findViewById(R.id.newgame);
        newgame.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent n = new Intent(HomeActivity.this,GameActivity.class);
            	n.putExtra("Games", xx);
            	HomeActivity.this.startActivity(n);
            	
            }
        });
        
        final Button recordedgames = (Button) findViewById(R.id.recordedgames);
        recordedgames.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent n = new Intent(HomeActivity.this,GameListActivity.class);
            	n.putExtra("Games", xx);
            	HomeActivity.this.startActivity(n);
            	
            }
        });
        
        
	}

	
}
