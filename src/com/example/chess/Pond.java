package com.example.chess;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * 
 * @author Brian
 * Pond object that extends Tile
 *
 */
public class Pond extends Tile implements Serializable{
/**
 * Constructor 
 * @param x
 * @param y
 * @param name
 */
	public Pond(int x, int y, String name) {
		super(x, y, name);
		// TODO Auto-generated constructor stub
	}
	/**
	 * get all the moves possible by a pond object
	 * Does not include enpassment in this method
	 * @param game
	 * @return ArrayList<Tile>
	 */
	public ArrayList<Tile> moves(Tile[][] game){
		ArrayList<Tile> r = new ArrayList<Tile>();
		if(name.charAt(0) == 'w'){
			int i = x;
			int k = y;
			if(i == 6 && game[5][y].name == ""){
				if(game[4][y].name == ""){
					Tile n = new Tile(8-(x-2),y,"");
					r.add(n);
				}
			}
			if(game[i-1][y].name == ""){
				Tile n = new Tile(8-(x-1),y,"");
				r.add(n);
			}
			if(i-1 >= 0 && k-1 >= 0){
				if(game[i-1][k-1].name != ""){
				if(game[i-1][k-1].name.charAt(0) == 'b' && game[i-1][k-1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(i-1),k-1,"");
					r.add(n);
				}
				}
			}
			if(i-1 >= 0 && k+1 < 8){
				if(game[i-1][k+1].name != ""){
				if(game[i-1][k+1].name.charAt(0) == 'b' && game[i-1][k+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(i-1),k+1,"");
					r.add(n);
				}
				}
			}
			
		}else{
			int i = x;
			int k = y;
			if(i == 1 && game[2][y].name == ""){
				if(game[3][y].name == ""){
					Tile n = new Tile(8-(i+2),y,"");
					r.add(n);
				}
			}
			if(game[i+1][y].name == ""){
				Tile n = new Tile(8-(i+1),y,"");
				r.add(n);
			}
			if(i+1 < 8 && k-1 >= 0){
				if(game[i+1][k-1].name != ""){
				if(game[i+1][k-1].name.charAt(0) == 'w' && game[i+1][k-1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(i+1),k-1,"");
					r.add(n);
				}
				}
			}
			if(i+1 < 8 && k+1 < 8){
				if(game[i+1][k+1].name != ""){
				if(game[i+1][k+1].name.charAt(0) == 'w' && game[i+1][k+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(i+1),k+1,"");
					r.add(n);
				}
				}
			}
		}
		return r;
	}
}
