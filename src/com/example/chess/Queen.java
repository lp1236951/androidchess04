package com.example.chess;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Queen object that extends the tile object
 * @author Anand
 *
 */
public class Queen extends Tile implements Serializable{
/**
 * Constructor 
 * @param x - location
 * @param y - location
 * @param name - either wQ or bQ
 */
	public Queen(int x, int y, String name) {
		super(x, y, name);
		// TODO Auto-generated constructor stub
	}
	/**
	 * Returns the possible moves of a queen object given the board state
	 * @param game - state of game
	 * @return ArrayList<Tile> - moves possible for the queen object
	 */
	public ArrayList<Tile> moves(Tile[][] game){
		ArrayList<Tile> r = new ArrayList<Tile>();
		for(int i = x-1, k = y-1; i >= 0; i--,k--){
			if(name.charAt(0) == 'w'){
				if(k < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}else{
				if(k < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}
		}
		
		for(int i = x-1, k = y+1; k < 8; i--,k++){
			if(name.charAt(0) == 'w'){
				if(i < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}else{
				if(i < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}
		}
		for(int i = x+1, k = y-1; i < 8; i++,k--){
			if(name.charAt(0) == 'w'){
				if(k < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}else{
				if(k < 0){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}
		}
		for(int i = x+1, k = y+1; i < 8; i++,k++){
			if(name.charAt(0) == 'w'){
				if(k >= 8){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}else{
				if(k >= 8){
					break;
				}
				if(game[i][k].name == ""){

					Tile n = new Tile(8-i,k,"");
					r.add(n);
					continue;
				}
				if(game[i][k].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][k].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][k].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,k,"");
					r.add(n);
					break;
				}
			}
		}
		for(int i = x-1; i >= 0; i--){
			if(name.charAt(0) == 'w'){
				if(game[i][y].name.compareTo("") == 0){

					Tile n = new Tile(8-i,y, "");
					r.add(n);
					continue;
				}
				if(game[i][y].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][y].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][y].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					break;
				}
			}else{
				if(game[i][y].name.compareTo("") == 0){

					Tile n = new Tile(8-i,y, "");
					r.add(n);
					continue;
				}
				if(game[i][y].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][y].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][y].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					break;
				}
				
			}
		}
		
		for(int i = x+1; i < 8; i++){
			if(name.charAt(0) == 'w'){
				if(game[i][y].name.compareTo("") == 0){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					continue;
				}
				if(game[i][y].name.charAt(0) == 'w'){
					break;
				}
				if(game[i][y].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][y].name.charAt(0) == 'b'){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					break;
				}
				
			}else{
				if(game[i][y].name.compareTo("") == 0){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					continue;
				}
				if(game[i][y].name.charAt(0) == 'b'){
					break;
				}
				if(game[i][y].name.charAt(1) == 'K'){
					break;
				}
				if(game[i][y].name.charAt(0) == 'w'){
					Tile n = new Tile(8-i,y, "");
					r.add(n);
					break;
				}
			}
		}
		for(int i = y-1; i >= 0; i--){
			if(name.charAt(0) == 'w'){
				if(game[x][i].name.compareTo("") == 0){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					continue;
				}
				if(game[x][i].name.charAt(0) == 'w'){
					break;
				}
				if(game[x][i].name.charAt(1) == 'K'){
					break;
				}
				if(game[x][i].name.charAt(0) == 'b'){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					break;
				}
			}else{
				if(game[x][i].name.compareTo("") == 0){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					continue;
				}
				if(game[x][i].name.charAt(0) == 'b'){
					break;
				}
				if(game[x][i].name.charAt(1) == 'K'){
					break;
				}
				if(game[x][i].name.charAt(0) == 'w'){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					break;
				}
			}
		}
		for(int i = y+1; i < 8; i++){
			if(name.charAt(0) == 'w'){
				if(game[x][i].name.compareTo("") == 0){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					continue;
				}
				if(game[x][i].name.charAt(0) == 'w'){
					break;
				}
				if(game[x][i].name.charAt(1) == 'K'){
					break;
				}
				if(game[x][i].name.charAt(0) == 'b'){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					break;
				}
			}else{
				if(game[x][i].name.compareTo("") == 0){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					continue;
				}
				if(game[x][i].name.charAt(0) == 'b'){
					break;
				}
				if(game[x][i].name.charAt(1) == 'K'){
					break;
				}
				if(game[x][i].name.charAt(0) == 'w'){
					Tile n = new Tile(8-x,i, "");
					r.add(n);
					break;
				}
			}
		}
		return r;
	}
}
