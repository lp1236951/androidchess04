package com.example.chess;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @author Brian
 * 
 */
public class Game implements Serializable{
	Tile[][] Board;
	/**
	 * Creates the game board
	 */
	public Game(){
	Board = new Tile[8][8];
	for(int i = 0; i < 8; i++){
		Board[1][i] = new Pond(1, i, "bp");
	}
	for(int i = 2; i < 6; i++){
		for(int k = 0; k < 8; k++){
			Board[i][k] = new Tile(i,k,"");
		}
	}
	for(int i = 0; i < 8; i++){
		Board[6][i] = new Pond(6, i, "wp");
	}
	Board[0][0] = new Castle(0, 0, "bR");
	Board[0][7] = new Castle(0, 7, "bR");
	Board[7][0] = new Castle(7, 0, "wR");
	Board[7][7] = new Castle(7, 7, "wR");
	Board[0][1] = new Knight(0, 1, "bN");
	Board[0][6] = new Knight(0, 6, "bN");
	Board[7][1] = new Knight(7, 1, "wN");
	Board[7][6] = new Knight(7, 6, "wN");
	Board[0][2] = new Bishop(0, 2, "bB");
	Board[0][5] = new Bishop(0, 5, "bB");
	Board[7][2] = new Bishop(7, 2, "wB");
	Board[7][5] = new Bishop(7, 5, "wB");
	Board[0][3] = new Queen(0, 3, "bQ");
	Board[7][3] = new Queen(7, 3, "wQ");
	Board[0][4] = new King(0, 4, "bK");
	Board[7][4] = new King(7, 4, "wK");
	

	}
	public void reset(){
		Board = new Tile[8][8];
		for(int i = 0; i < 8; i++){
			Board[1][i] = new Pond(1, i, "bp");
		}
		for(int i = 2; i < 6; i++){
			for(int k = 0; k < 8; k++){
				Board[i][k] = new Tile(i,k,"");
			}
		}
		for(int i = 0; i < 8; i++){
			Board[6][i] = new Pond(6, i, "wp");
		}
		Board[0][0] = new Castle(0, 0, "bR");
		Board[0][7] = new Castle(0, 7, "bR");
		Board[7][0] = new Castle(7, 0, "wR");
		Board[7][7] = new Castle(7, 7, "wR");
		Board[0][1] = new Knight(0, 1, "bN");
		Board[0][6] = new Knight(0, 6, "bN");
		Board[7][1] = new Knight(7, 1, "wN");
		Board[7][6] = new Knight(7, 6, "wN");
		Board[0][2] = new Bishop(0, 2, "bB");
		Board[0][5] = new Bishop(0, 5, "bB");
		Board[7][2] = new Bishop(7, 2, "wB");
		Board[7][5] = new Bishop(7, 5, "wB");
		Board[0][3] = new Queen(0, 3, "bQ");
		Board[7][3] = new Queen(7, 3, "wQ");
		Board[0][4] = new King(0, 4, "bK");
		Board[7][4] = new King(7, 4, "wK");
		
	}
	
	public boolean castling(int e1, int e2, int c1, int c2){
		if(Board[c1][c2].name.charAt(1) == 'K'){
			if(e1 == 7 && e2 == 2){
				if(Board[7][0].name == "wR"){
					if(Board[7][1].name == "" && Board[7][2].name == "" && Board[7][3].name == ""){
						Board[7][3] = new Castle(7,3,"wR");
						Board[7][2] = new King(7,2,"wK");
						Board[7][0] = new Tile(7,0,"");
						Board[7][4] = new Tile(7,4,"");
						return true;
						
					}
				}
			}
			if(e1 == 7 && e2 == 6){
				if(Board[7][7].name == "wR"){
					if(Board[7][6].name == "" && Board[7][5].name == ""){
						Board[7][5] = new Castle(7,5,"wR");
						Board[7][6] = new King(7,6,"wK");
						Board[7][7] = new Tile(7,0,"");
						Board[7][4] = new Tile(7,4,"");
						return true;
					}
				}
			}
			if(e1 == 0 && e2 == 6){
				if(Board[0][7].name == "bR"){
					if(Board[0][6].name == "" && Board[0][5].name == ""){
						Board[0][5] = new Castle(0,5,"bR");
						Board[0][6] = new King(0,6,"bK");
						Board[0][7] = new Tile(0,0,"");
						Board[0][4] = new Tile(0,4,"");
						return true;
					}
				}
			}
			if(e1 == 0 && e2 == 2){
				if(Board[0][0].name == "bR"){
					if(Board[0][1].name == "" && Board[0][2].name == "" && Board[0][3].name == ""){
						Board[0][3] = new Castle(0,3,"bR");
						Board[0][2] = new King(0,2,"bK");
						Board[0][0] = new Tile(0,0,"");
						Board[0][4] = new Tile(0,4,"");
						return true;
					}
				}
			}
		}
		return false;
	}
	public ArrayList<Tile> moves(int c1, int c2) {
		return Board[c1][c2].moves(Board);
		
	}
	public String getname(int c1, int c2) {
		return Board[c1][c2].name;
	}
	public void setp(int e1, int e2, String p){
		if(p.charAt(1) == 'p'){
			Board[e1][e2] = new Pond(e1, e2, p);
		}
		if(p.charAt(1) == 'R'){
			Board[e1][e2] = new Castle(e1, e2, p);
		}
		if(p.charAt(1) == 'N'){
			Board[e1][e2] = new Knight(e1, e2, p);
		}
		if(p.charAt(1) == 'B'){
			Board[e1][e2] = new Bishop(e1, e2, p);
		}
		if(p.charAt(1) == 'Q'){
			Board[e1][e2] = new Queen(e1, e2, p);
		}
		if(p.charAt(1) == 'K'){
			Board[e1][e2] = new King(e1, e2, p);
		}
	}
	public void remove(int e1, int e2){
		Board[e1][e2] = new Tile(e1,e2,"");
	}
}
