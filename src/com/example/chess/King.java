package com.example.chess;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * 
 * @author Anand
 * 
 */
public class King extends Tile implements Serializable{
	/**
	 * Field moved is to check if castling is possible
	 */
	boolean moved = false;
	/**
	 * Constructor 
	 * @param x
	 * @param y
	 * @param name
	 */
	public King(int x, int y, String name) {
		super(x, y, name);
		// TODO Auto-generated constructor stub
	}
	/**
	 * Given the current board state, the methods finds possible moves of the king object
	 * @param game
	 * @return ArrayList<Tile>
	 */
	public ArrayList<Tile> moves(Tile[][] game){
		ArrayList<Tile> r = new ArrayList<Tile>();
		if(name.charAt(0) == 'w'){
			if(x+1 < 8 && y+1 < 8){
				if(game[x+1][y+1].name == ""){
					Tile n = new Tile(8-(x+1),y+1,"");
					r.add(n);
				}else if(game[x+1][y+1].name.charAt(0) == 'b' && game[x+1][y+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x+1),y+1,"");
					r.add(n);
				}
			}
			if(x+1 < 8 && y-1 >= 0){
				if(game[x+1][y-1].name == ""){
					Tile n = new Tile(8-(x+1),y-1,"");
					r.add(n);
				}else
				if(game[x+1][y-1].name.charAt(0) == 'b' && game[x+1][y-1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x+1),y-1,"");
					r.add(n);
				}
			}
			if(x-1 >= 0 && y+1 < 8){
				if(game[x-1][y+1].name == ""){
					Tile n = new Tile(8-(x-1),y+1,"");
					r.add(n);
				}else
				if(game[x-1][y+1].name.charAt(0) == 'b' && game[x-1][y+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x-1),y+1,"");
					r.add(n);
				}
			}
			if(x-1 >= 0 && y-1 >= 0){
				if(game[x-1][y-1].name == ""){
					Tile n = new Tile(8-(x-1),y-1,"");
					r.add(n);
				}else
				if(game[x-1][y-1].name.charAt(0) == 'b' && game[x-1][y-1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x-1),y-1,"");
					r.add(n);
				}
			}
			if(x+1 < 8){
				if(game[x+1][y].name == ""){
					Tile n = new Tile(8-(x+1),y,"");
					r.add(n);
				}else
				if(game[x+1][y].name.charAt(0) == 'b' && game[x+1][y].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x+1),y,"");
					r.add(n);
				}
			}
			if(y+1 < 8){
				if(game[x][y+1].name == ""){
					Tile n = new Tile(8-x,y+1,"");
					r.add(n);
				}else
				if(game[x][y+1].name.charAt(0) == 'b' && game[x][y+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-x,y+1,"");
					r.add(n);
				}
			}
			if(x-1 >= 0){
				if(game[x-1][y].name == ""){
					Tile n = new Tile(8-(x-1),y,"");
					r.add(n);
				}else
				if(game[x-1][y].name.charAt(0) == 'b' && game[x-1][y].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x-1),y,"");
					r.add(n);
				}
			}
			if(y-1 >= 0){
				if(game[x][y-1].name == ""){
					Tile n = new Tile(8-x,y-1,"");
					r.add(n);
				}else
				if(game[x][y-1].name.charAt(0) == 'b' && game[x][y-1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-x,y-1,"");
					r.add(n);
				}
			}
		}else{
			if(x+1 < 8 && y+1 < 8){
				if(game[x+1][y+1].name == ""){
					Tile n = new Tile(8-(x+1),y+1,"");
					r.add(n);
				}else
				if(game[x+1][y+1].name.charAt(0) == 'w' && game[x+1][y+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x+1),y+1,"");
					r.add(n);
				}
			}
			if(x+1 < 8 && y-1 >= 0){
				if(game[x+1][y-1].name == ""){
					Tile n = new Tile(8-(x+1),y-1,"");
					r.add(n);
				}else
				if(game[x+1][y-1].name.charAt(0) == 'w' && game[x+1][y-1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x+1),y-1,"");
					r.add(n);
				}
			}
			if(x-1 >= 0 && y+1 < 8){
				if(game[x-1][y+1].name == ""){
					Tile n = new Tile(8-(x-1),y+1,"");
					r.add(n);
				}else
				if(game[x-1][y+1].name.charAt(0) == 'w' && game[x-1][y+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x-1),y+1,"");
					r.add(n);
				}
			}
			if(x-1 >= 0 && y-1 >= 0){
				if(game[x-1][y-1].name == ""){
					Tile n = new Tile(8-(x-1),y-1,"");
					r.add(n);
				}else
				if(game[x-1][y-1].name.charAt(0) == 'w' && game[x-1][y-1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x-1),y-1,"");
					r.add(n);
				}
			}
			if(x+1 < 8){
				if(game[x+1][y].name == ""){
					Tile n = new Tile(8-(x+1),y,"");
					r.add(n);
				}else
				if(game[x+1][y].name.charAt(0) == 'w' && game[x+1][y].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x+1),y,"");
					r.add(n);
				}
			}
			if(y+1 < 8){
				if(game[x][y+1].name == ""){
					Tile n = new Tile(8-(x),y+1,"");
					r.add(n);
				}else
				if(game[x][y+1].name.charAt(0) == 'w' && game[x][y+1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x),y+1,"");
					r.add(n);
				}
			}
			if(x-1 >= 0){
				if(game[x-1][y].name == ""){
					Tile n = new Tile(8-(x-1),y,"");
					r.add(n);
				}else
				if(game[x-1][y].name.charAt(0) == 'w' && game[x-1][y].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x-1),y,"");
					r.add(n);
				}
			}
			if(y-1 >= 0){
				if(game[x][y-1].name == ""){
					Tile n = new Tile(8-(x),y-1,"");
					r.add(n);
				}else
				if(game[x][y-1].name.charAt(0) == 'w' && game[x][y-1].name.charAt(1) != 'K'){
					Tile n = new Tile(8-(x),y-1,"");
					r.add(n);
				}
			}
		}
		return r;
	}
	
	public void moved(){
		moved = true;
	}

}
