package com.example.chess;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class playgame_activity extends Activity {
	
	ArrayList<Node> f;
	Node y;
	int c = 0;
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.playgame);
		f = (ArrayList<Node>)getIntent().getSerializableExtra("Games");
		y = f.get(0);
		final Button prev = (Button)findViewById(R.id.back);
		prev.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(c != 0){
					c--;
				}else{
					return;
				}
				y = f.get(c);
				Tile[][] temp = y.board;
				Tile[][] board = new Tile[8][8];
				for(int i = 0; i < 8; i++){
            		for(int k = 0; k < 8; k++){
            			board[i][k] = temp[i][k];
            		}
            	}
            	
            	ImageView h = (ImageView) findViewById(R.id.p00);
            	if(board[0][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p01);
            	if(board[0][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p02);
            	if(board[0][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p03);
            	if(board[0][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p04);
            	if(board[0][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p05);
            	if(board[0][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p06);
            	if(board[0][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p07);
            	if(board[0][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	
            	h = (ImageView) findViewById(R.id.p10);
            	if(board[1][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p11);
            	if(board[1][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p12);
            	if(board[1][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p13);
            	if(board[1][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p14);
            	if(board[1][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p15);
            	if(board[1][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p16);
            	if(board[1][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p17);
            	if(board[1][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p20);
            	if(board[2][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p21);
            	if(board[2][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p22);
            	if(board[2][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p23);
            	if(board[2][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p24);
            	if(board[2][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p25);
            	if(board[2][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p26);
            	if(board[2][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p27);
            	if(board[2][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p30);
            	if(board[3][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p31);
            	if(board[3][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p32);
            	if(board[3][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p33);
            	if(board[3][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p34);
            	if(board[3][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p35);
            	if(board[3][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p36);
            	if(board[3][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p37);
            	if(board[3][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p40);
            	if(board[4][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p41);
            	if(board[4][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p42);
            	if(board[4][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p43);
            	if(board[4][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p44);
            	if(board[4][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p45);
            	if(board[4][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p46);
            	if(board[4][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p47);
            	if(board[4][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p50);
            	if(board[5][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p51);
            	if(board[5][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p52);
            	if(board[5][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p53);
            	if(board[5][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p54);
            	if(board[5][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p55);
            	if(board[5][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p56);
            	if(board[5][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p57);
            	if(board[5][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p60);
            	if(board[6][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p61);
            	if(board[6][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p62);
            	if(board[6][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p63);
            	if(board[6][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p64);
            	if(board[6][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p65);
            	if(board[6][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p66);
            	if(board[6][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p67);
            	if(board[6][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p70);
            	if(board[7][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p71);
            	if(board[7][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p72);
            	if(board[7][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p73);
            	if(board[7][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p74);
            	if(board[7][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p75);
            	if(board[7][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p76);
            	if(board[7][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p77);
            	if(board[7][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
				
			}
			
			
		});
		final Button next = (Button)findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(c != f.size()){
					c++;
				}else{
					return;
				}
				y = f.get(c);
				Tile[][] temp = y.board;
				Tile[][] board = new Tile[8][8];
				for(int i = 0; i < 8; i++){
            		for(int k = 0; k < 8; k++){
            			board[i][k] = temp[i][k];
            		}
            	}
            	
            	ImageView h = (ImageView) findViewById(R.id.p00);
            	if(board[0][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p01);
            	if(board[0][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p02);
            	if(board[0][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p03);
            	if(board[0][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p04);
            	if(board[0][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p05);
            	if(board[0][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p06);
            	if(board[0][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p07);
            	if(board[0][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[0][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[0][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[0][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[0][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[0][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[0][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[0][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[0][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[0][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[0][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[0][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[0][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	
            	h = (ImageView) findViewById(R.id.p10);
            	if(board[1][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p11);
            	if(board[1][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p12);
            	if(board[1][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p13);
            	if(board[1][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p14);
            	if(board[1][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p15);
            	if(board[1][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p16);
            	if(board[1][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p17);
            	if(board[1][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[1][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[1][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[1][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[1][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[1][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[1][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[1][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[1][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[1][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[1][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[1][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[1][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p20);
            	if(board[2][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p21);
            	if(board[2][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p22);
            	if(board[2][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p23);
            	if(board[2][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p24);
            	if(board[2][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p25);
            	if(board[2][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p26);
            	if(board[2][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p27);
            	if(board[2][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[2][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[2][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[2][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[2][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[2][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[2][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[2][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[2][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[2][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[2][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[2][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[2][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p30);
            	if(board[3][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p31);
            	if(board[3][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p32);
            	if(board[3][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p33);
            	if(board[3][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p34);
            	if(board[3][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p35);
            	if(board[3][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p36);
            	if(board[3][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p37);
            	if(board[3][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[3][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[3][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[3][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[3][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[3][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[3][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[3][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[3][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[3][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[3][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[3][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[3][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p40);
            	if(board[4][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p41);
            	if(board[4][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p42);
            	if(board[4][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p43);
            	if(board[4][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p44);
            	if(board[4][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p45);
            	if(board[4][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p46);
            	if(board[4][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p47);
            	if(board[4][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[4][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[4][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[4][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[4][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[4][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[4][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[4][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[4][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[4][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[4][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[4][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[4][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p50);
            	if(board[5][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p51);
            	if(board[5][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p52);
            	if(board[5][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p53);
            	if(board[5][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p54);
            	if(board[5][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p55);
            	if(board[5][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p56);
            	if(board[5][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p57);
            	if(board[5][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[5][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[5][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[5][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[5][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[5][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[5][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[5][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[5][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[5][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[5][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[5][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[5][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p60);
            	if(board[6][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p61);
            	if(board[6][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p62);
            	if(board[6][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p63);
            	if(board[6][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p64);
            	if(board[6][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p65);
            	if(board[6][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p66);
            	if(board[6][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p67);
            	if(board[6][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[6][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[6][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[6][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[6][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[6][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[6][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[6][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[6][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[6][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[6][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[6][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[6][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p70);
            	if(board[7][0].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][0].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][0].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][0].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][0].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][0].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][0].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][0].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][0].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][0].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][0].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][0].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][0].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p71);
            	if(board[7][1].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][1].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][1].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][1].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][1].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][1].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][1].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][1].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][1].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][1].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][1].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][1].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][1].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p72);
            	if(board[7][2].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][2].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][2].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][2].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][2].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][2].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][2].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][2].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][2].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][2].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][2].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][2].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][2].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p73);
            	if(board[7][3].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][3].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][3].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][3].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][3].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][3].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][3].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][3].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][3].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][3].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][3].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][3].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][3].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p74);
            	if(board[7][4].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][4].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][4].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][4].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][4].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][4].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][4].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][4].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][4].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][4].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][4].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][4].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][4].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p75);
            	if(board[7][5].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][5].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][5].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][5].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][5].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][5].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][5].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][5].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][5].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][5].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][5].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][5].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][5].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p76);
            	if(board[7][6].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][6].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][6].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][6].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][6].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][6].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][6].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][6].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][6].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][6].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][6].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][6].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][6].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
            	h = (ImageView) findViewById(R.id.p77);
            	if(board[7][7].name == "wp"){
            		h.setImageResource(R.drawable.w_p);
            	}
            	if(board[7][7].name == "bp"){
            		h.setImageResource(R.drawable.b_p);
            	}
            	if(board[7][7].name == "wK"){
            		h.setImageResource(R.drawable.w_k);
            	}
            	if(board[7][7].name == "bK"){
            		h.setImageResource(R.drawable.b_k);
            	}
            	if(board[7][7].name == "wR"){
            		h.setImageResource(R.drawable.w_r);
            	}
            	if(board[7][7].name == "bR"){
            		h.setImageResource(R.drawable.b_r);
            	}
            	if(board[7][7].name == "wN"){
            		h.setImageResource(R.drawable.w_n);
            	}
            	if(board[7][7].name == "bN"){
            		h.setImageResource(R.drawable.b_n);
            	}
            	if(board[7][7].name == "wB"){
            		h.setImageResource(R.drawable.w_b);
            	}
            	if(board[7][7].name == "bB"){
            		h.setImageResource(R.drawable.b_b);
            	}
            	if(board[7][7].name == "wQ"){
            		h.setImageResource(R.drawable.w_q);
            	}
            	if(board[7][7].name == "bQ"){
            		h.setImageResource(R.drawable.b_q);
            	}
            	if(board[7][7].name == ""){
            		h.setImageResource(R.drawable.bla_nk);
            	}
				
			}
			
			
		});
			
	}
}